import json

import django.http
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.http import quote, unquote
from django.utils.translation import ugettext as _
from django.views import View
from django.views.generic import CreateView, UpdateView, ListView, DetailView

from testsapp.permissions import UserOwnsObjectMixin, ensure_user_owns_object
from .forms import ChoiceInlineFormSet, QuestionForm
from .models import Question, Test


class IndexView(View):
    def get(self, request):
        return render(request, "index.html", {})


def create_edit_question(
        request,
        question_pk=None,
        test_pk=None,
        single_correct_answer=False
):
    """
    :param request:
    :param question_pk: Question's primary key. Should be not None for edit
    view.
    :param test_pk: Test's primary key. Should be not None for create view.
    :param single_correct_answer: Should question have one or multiple
    correct answers.
    :return:
    """
    data = {}
    if question_pk:
        action = _('edit')
        question = get_object_or_404(Question, pk=question_pk)
        test = question.test
    else:
        action = _('create')
        test = get_object_or_404(Test, pk=test_pk)
        question = Question(test=test,
                            only_one_correct_choice=single_correct_answer)

    ensure_user_owns_object(request.user, test)

    if request.method == "POST":
        question_form = QuestionForm(request.POST, instance=question)
        if question_form.is_valid():
            created_question = question_form.save(commit=False)
            formset = ChoiceInlineFormSet(request.POST,
                                          instance=created_question)
            if formset.is_valid():
                created_question.save()
                formset.save()
                data['form_is_valid'] = True
                questions = test.questions.all()
                data['html_container_data'] = render_to_string(
                    'testsapp/partials/questions.html',
                    {'questions': questions}
                )
            else:
                data['form_is_valid'] = False
        else:
            data['form_is_valid'] = False
            formset = ChoiceInlineFormSet(request.POST, instance=question)
    else:
        question_form = QuestionForm(instance=question)
        formset = ChoiceInlineFormSet(instance=question)
    context = {
        'choice_formset': formset,
        'question_form': question_form,
        'action': action,
        'question': question,
        'test': test,
    }
    data['html_form'] = render_to_string(
        'testsapp/partials/question_form.html', context,
        request)
    return JsonResponse(data)


def delete_question(request, pk):
    question = get_object_or_404(Question, pk=pk)
    ensure_user_owns_object(request.user, question)
    if request.method == 'POST':
        data = {'form_is_valid': True}
        questions = question.test.questions.all()
        question.delete()
        data['html_container_data'] = render_to_string(
            'testsapp/partials/questions.html',
            {'questions': questions})
        return JsonResponse(data)
    context = {'question': question}
    data = {'html_form': render_to_string(
        'testsapp/partials/question_confirm_delete.html', context, request)}
    return JsonResponse(data)


class TestCreateView(LoginRequiredMixin, CreateView):
    model = Test

    fields = ('title',)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        return django.http.HttpResponseRedirect(self.get_success_url())


class TestUpdateView(UserOwnsObjectMixin, UpdateView):
    model = Test

    fields = ('title',)


def test_delete(request, pk):
    test = get_object_or_404(Test, pk=pk)
    ensure_user_owns_object(request.user, test)

    if request.method == 'POST':
        test.delete()
        return redirect('user-test-list')
    context = {'test': test}
    data = {'html_form': render_to_string(
        'testsapp/partials/test_confirm_delete.html', context, request)}
    return JsonResponse(data)


class TestListView(ListView):
    paginate_by = 10
    queryset = Test.objects.filter(questions__isnull=False).distinct()


class UserTestListView(LoginRequiredMixin, ListView):
    paginate_by = 2

    def get_queryset(self):
        user = self.request.user
        return Test.objects.filter(owner=user)


class TestEditView(UserOwnsObjectMixin, DetailView):
    template_name = 'testsapp/test_edit.html'
    model = Test


def _store(d):
    return quote(json.dumps(d))


def _load(s):
    return json.loads(unquote(s))


def test_result(request, pk, answers):
    answers = _load(answers)
    answers = {int(k): list(map(int, v)) for k, v in answers.items()}

    test = get_object_or_404(Test, pk=pk)
    right_choices, wrong_choices = test.check_answers(answers)
    errors_amount = sum(1 for q, choices in wrong_choices.items() if choices)
    question_amount = len(test.questions.all())

    checked_choices = {id for choices in answers.values() for id in choices}
    context = {
        'test': test,
        'right_answers_amount': question_amount - errors_amount,
        'question_amount': question_amount,
        'right_choices': right_choices,
        'checked_choices': checked_choices,
    }
    return render(request, 'testsapp/test_result.html', context)


def take_test(request, pk):
    if request.method == "POST":
        question_id = request.POST['question']
        answers = request.POST.getlist('answers')
        answered_questions = _load(request.POST['answered_question'])
        answered_questions[question_id] = answers
    else:
        answered_questions = {}

    test = get_object_or_404(Test, pk=pk)

    question = next(test.get_unanswered_questions(answered_questions), None)

    if question is None:
        kwargs = {'pk': test.pk, 'answers': _store(answered_questions)}
        return redirect(reverse('test-result', kwargs=kwargs))

    context = {
        'question': question,
        'answered_question': _store(answered_questions),
    }
    return render(request, 'testsapp/test_process.html', context)
