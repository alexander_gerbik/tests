from django.conf.urls import url

from testsapp.views import delete_question, create_edit_question, \
    TestCreateView, TestUpdateView, TestListView, \
    UserTestListView, TestEditView, take_test, test_result, test_delete

urlpatterns = [
    url(r'^$', TestListView.as_view(), name='test-list'),
    url(r'^mine/$', UserTestListView.as_view(), name='user-test-list'),
    url(r'^(?P<pk>[0-9]+)/$', TestEditView.as_view(), name='test-edit'),
    url(r'^question/edit/(?P<question_pk>[0-9]+)/$',
        create_edit_question,
        name='question-edit'),
    url(r'^question/delete/(?P<pk>[0-9]+)/$',
        delete_question,
        name='question-delete'),
    url(r'^(?P<test_pk>[0-9]+)/question/add/$', create_edit_question,
        name='question-create'),
    url(r'^(?P<test_pk>[0-9]+)/question/add/single-choice/$',
        create_edit_question, kwargs={'single_correct_answer': True},
        name='question-create-single-choice'),
    url(r'^add/$', TestCreateView.as_view(), name='test-create'),
    url(r'^edit/(?P<pk>[0-9]+)/$', TestUpdateView.as_view(), name='test-update'),
    url(r'^delete/(?P<pk>[0-9]+)/$', test_delete,
        name='test-delete'),
    url(r'^take/(?P<pk>[0-9]+)/$', take_test, name='test-take'),
    url(r'^result/(?P<pk>[0-9]+)/(?P<answers>.+)/$', test_result,
        name='test-result'),
]
