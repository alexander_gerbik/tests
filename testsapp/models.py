from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy


class Test(models.Model):
    class Meta:
        ordering = ('title',)

    title = models.CharField(max_length=100)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False,
                              on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('test-edit', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}(title={})'.format(self.__class__.__name__, self.title)

    def get_unanswered_questions(self, answered_questions):
        for question in self.questions.all():
            if (question.id in answered_questions or
                        str(question.id) in answered_questions):
                continue
            yield question

    def check_answers(test, answers):
        right_choices = {}
        wrong_choices = {}
        for question in test.questions.all():
            right_chosen, wrong_chosen = question.check_answer(
                answers[question.id])
            right_choices[question.id] = right_chosen
            wrong_choices[question.id] = wrong_chosen
        return right_choices, wrong_choices


class Question(models.Model):
    text = models.TextField(verbose_name=ugettext_lazy('question'))
    test = models.ForeignKey(Test, on_delete=models.CASCADE,
                             related_name='questions')
    only_one_correct_choice = models.BooleanField(default=False)

    @property
    def owner(self):
        return self.test.owner

    def __str__(self):
        return '{}(text={})'.format(self.__class__.__name__, self.text)

    def check_answer(self, chosen):
        chosen_right = set()
        chosen_wrong = set()
        for choice in self.choices.all():
            if choice.is_correct == (choice.id in chosen):
                chosen_right.add(choice.id)
            else:
                chosen_wrong.add(choice.id)
        return chosen_right, chosen_wrong


class Choice(models.Model):
    text = models.TextField(verbose_name=ugettext_lazy('answer choice'))
    is_correct = models.BooleanField(verbose_name=ugettext_lazy('correct'))
    question = models.ForeignKey(Question,
                                 related_name='choices',
                                 on_delete=models.CASCADE)

    def __str__(self):
        return '{}(text={})'.format(self.__class__.__name__, self.text)
