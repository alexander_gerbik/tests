from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def active_url(context, url):
    link_url = reverse(url)
    current_url = context['request'].path
    if current_url == link_url:
        return 'active'
    return ''
