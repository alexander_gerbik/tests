# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-24 07:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testsapp', '0004_auto_20171124_0733'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='only_one_correct_choice',
            field=models.BooleanField(default=False),
        ),
    ]
