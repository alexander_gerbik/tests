from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from .models import Question, Choice


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        exclude = ()
        widgets = {
            'test': forms.HiddenInput(),
            'only_one_correct_choice': forms.HiddenInput(),
        }


class ChoiceFormSet(forms.BaseInlineFormSet):
    def clean(self):
        super().clean()
        correct = [form.cleaned_data['is_correct'] for form in self.forms
                   if 'is_correct' in form.cleaned_data
                   and not form.cleaned_data['DELETE']]
        if len(correct) < 2:
            raise ValidationError(_("Question should have at least two "
                                    "possible choices."),
                                  code="not_enough_choices")
        if all(correct):
            raise ValidationError(_("Question should have at least one "
                                    "incorrect answer."),
                                  code="missing_incorrect_answer")
        if not any(correct):
            raise ValidationError(_("Question should have at least one "
                                    "correct answer."),
                                  code="missing_correct_answer")
        if (self.instance.only_one_correct_choice
            and sum(1 for c in correct if c) != 1):
            raise ValidationError(_("Single choice question should have"
                                    " exactly one correct answer."),
                                  code="should_have_one_correct_answer")


ChoiceInlineFormSet = forms.inlineformset_factory(
    Question,
    Choice,
    extra=1,
    can_delete=True,
    fields=('text', 'is_correct'),
    formset=ChoiceFormSet,
)
