from django.contrib.auth.mixins import AccessMixin
from django.core.exceptions import PermissionDenied


class UserOwnsObjectMixin(AccessMixin):
    """
        CBV mixin which verifies that the current user is owner of
        manipulated object.
        """
    owner_field_name = 'owner'

    def dispatch(self, request, *args, **kwargs):
        object = self.get_object()
        owner = getattr(object, self.owner_field_name)
        if request.user != owner:
            return self.handle_no_permission()
        return super(UserOwnsObjectMixin, self).dispatch(request, *args,
                                                         **kwargs)


def ensure_user_owns_object(user, object, owner_field_name='owner'):
    owner = getattr(object, owner_field_name)
    if user != owner:
        raise PermissionDenied("You can't perform this action as you are not"
                               " a owner of the object")
