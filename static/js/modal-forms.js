loadForm = function () {
    var btn = $(this);
    var modalId = btn.attr("data-modal-id");
    $.ajax({
        url: btn.attr("data-url"),
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            $("#" + modalId).modal("show");
        },
        success: function (data) {
            $("#" + modalId + " .modal-content").html(data.html_form);
        }
    });
};

saveForm = function () {
    var form = $(this);
    var modalId = form.attr("data-modal-id");
    var containerId = form.attr("data-container");
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: form.attr("method"),
        dataType: "json",
        success: function (data) {
            if (data.form_is_valid) {
                $("#" + containerId).html(data.html_container_data);
                $("#" + modalId).modal("hide");
            } else {
                $("#" + modalId + " .modal-content").html(data.html_form);
            }
        }
    });
    return false;
};