$(function () {

    $(".js-delete-test").click(loadForm);

    $(".js-add-question").click(loadForm);
    $("#questions-container").on("click", ".js-edit-question", loadForm);
    $("#modal-test").on("submit", ".js-create-edit-question-form", saveForm);

    $("#questions-container").on("click", ".js-delete-question", loadForm);
    $("#modal-test").on("submit", ".js-delete-question-form", saveForm);
});